class Post
  include Mongoid::Document

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h

  mount_uploader :look_image, LookImageUploader

  after_update :crop_image_look

  belongs_to :source
  embeds_many :images

  field :url, type: String
  field :published, type: Time

  validates :url, :published, presence: true
  validates_uniqueness_of :url

  def crop_image_look
    look_image.recreate_versions! if crop_x.present?
  end

  state_machine initial: :standby do

    event :look_created do
      transition :in_line => :created
    end

    event :get_in_line do
      transition :standby => :in_line
    end

    event :reject do
      transition :standby => :rejected
    end

    event :put_on_standby do
      transition [ :rejected, :in_line ] => :standby
    end

  end

end
