class Image
  include Mongoid::Document

  embedded_in :post

  field :src, type: String

  validates :src, presence: true, uniqueness: true
end
