class Source 
  include Mongoid::Document

  has_many :posts

  field :title, type: String
  field :category, type: String
  field :feed_url, type: String

  validates :category, :title, :feed_url, presence: true

end

