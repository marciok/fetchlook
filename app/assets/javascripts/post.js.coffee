jQuery ->

  $('.amp-look').click ->
    $('.imgs-select-container, .post-actions').hide()
    $('#crop-holder').show()
    $('.look-img').attr 'src', $(this).children().attr 'src'
    $('#post_remote_look_image_url').attr 'value', $('.look-img').attr 'src'
    new ImageLookCropper()

  $('.close-crop').click ->
    $('#crop-holder, .imgs-select-container, .post-actions').toggle()

class ImageLookCropper
  root = exports ? this

  constructor: ->
    if root.jcrop_api?
      root.jcrop_api.destroy()
    $('#cropbox').Jcrop
      onSelect: @update
      onChange: @update,
        ->
          root.jcrop_api = this

  update: (coords) =>
    $('#post_crop_x').val(coords.x)
    $('#post_crop_y').val(coords.y)
    $('#post_crop_w').val(coords.w)
    $('#post_crop_h').val(coords.h)

