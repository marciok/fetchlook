class CreateLook

  include Sidekiq::Worker
  def perform(params,id)
    post = Post.find(id)
    con = Faraday.new(url: ENV['LOOK_API_ROOT']){ |faraday| faraday.adapter(Faraday.default_adapter) }
    resp = con.post do |req|
      req.url '/1/looks.json'
      req.headers['Content-Type'] = 'application/json'
      req.body = ActiveSupport::JSON.encode({
        source_title: post.source.title,
        picture: post.look_image.adjusted.url,
        thumb: post.look_image.thumb.url,
        garments: params['garments'],
        occasions: params['occasions'],
        feed_url: post.source.feed_url,
        url: post.url
      })
    end
    if resp.status == 201
      post.look_created
    else
      post.put_on_standby
    end
  end

end
