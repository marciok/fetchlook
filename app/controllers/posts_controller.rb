class PostsController < ApplicationController 

  def index
    @source = Source.find(params[:source_id])
    @posts = @source.posts.order_by(state: -1, published: -1)
  end

  def show
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(params[:post])
      redirect_to source_post_path(@post.source, @post)
    end
  end

  def moderate
    @post = Post.find(params[:post_id])
    case params[:state]
    when 'Get in line'
      CreateLook.perform_at(10.seconds.from_now,params,@post.id)
      @post.get_in_line
    when 'Reject'
      @post.reject
    when 'Undo Reject'
      @post.put_on_standby
    end
    if @post.save!
      redirect_to source_posts_path(@post.source_id)
    else
      #TODO
    end
  end

end
