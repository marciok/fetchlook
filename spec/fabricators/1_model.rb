Fabricator(:source) do
  title { Faker::Name.name }
  category { Faker::Name.name }
  feed_url { Faker::Internet.url }
end

Fabricator(:image) do
  path { Faker::Internet.url }
end

Fabricator(:post) do
  url { Faker::Internet.url }
end
