require 'spec_helper'

describe Post do
  it { should be_a_kind_of(Mongoid::Document) }

  describe 'Relations' do
    it { should belong_to(:source) }
    it { should embed_many(:images) }
  end

  describe 'Attributes' do
    it { should have_field(:url).of_type(String) }
    it { should have_field(:published).of_type(Time) }
  end

  describe 'Validation' do
    it { should validate_presence_of(:url) }
    it { should validate_presence_of(:published) }
  end
end


