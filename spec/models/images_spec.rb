require 'spec_helper'

describe Image do
  it { should be_a_kind_of(Mongoid::Document) }

  describe 'Relations' do
    it { should be_embedded_in(:post) }
  end

  describe 'Attributes' do
    it { should have_field(:src).of_type(String) }
  end

  describe 'Validation' do
    it { should validate_presence_of(:src) }
  end
end



