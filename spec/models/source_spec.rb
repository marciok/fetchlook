require 'spec_helper'

describe Source do
  it { should be_a_kind_of(Mongoid::Document) }

  describe 'Realations' do
    it { should have_many(:posts) }
  end

  describe 'Attributes' do
    it { should have_field(:title).of_type(String) }
    it { should have_field(:category).of_type(String) }
    it { should have_field(:feed_url).of_type(String) }
  end

  describe 'Validation' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:feed_url) }
    it { should validate_presence_of(:category) }
  end
end

