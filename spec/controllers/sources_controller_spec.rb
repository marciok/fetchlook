require 'spec_helper.rb'

describe SourcesController do

  describe 'GET index' do
    let(:test_request) { get :index }

    before { test_request }

    let(:sources) { [ Fabricate(:source) ] }

    it 'assigns the Source collection as @sources' do
      assigns(:sources).should eq(sources)
    end

    it 'renders the Source collection' do
      response.should render_template(:index)
    end

    it 'returns HTTP OK' do
      response.code.should eq('200')
    end
  end
end

