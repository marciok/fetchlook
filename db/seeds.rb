# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#


# Seeding the sources

PICS_ON_SUMMARY = [
  'http://glamour.tumblr.com/',
  'http://facehunter.blogspot.com/',
  'http://hel-looks.com',
  'http://thelocals.dk',              
  'http://www.bostonstreetstyle.com/',
  'http://leblogdebetty.tumblr.com/'
]

bloggers_sites = %w{
  http://www.atlantic-pacific.blogspot.com.br/
  http://www.blogdapaulinha.com.br/
  http://www.blogdamariasophia.com/
  http://www.blogdathassia.com.br/br/
  http://www.comdrama.com.br/
  http://www.fashiontoast.com/
  http://www.fashionvibe.net/
  http://www.lalarudge.com.br/
  http://www.lasmimas.com.br/blog/
  http://www.manrepeller.com/
  http://www.songofstyle.blogspot.com.br/
  http://www.blogdamariah.com.br/
  http://www.glam4you.com/
  http://www.garotasestupidas.com/
  http://www.lalanoleto.com.br/
  http://www.hanneli.com/
  http://www.cupcakesandcashmere.com/
  http://www.theblondesalad.com/
  http://www.tuulavintage.com/
  http://www.thecherryblossomgirl.com/
  http://www.stylescrapbook.com/
  http://www.leblogdebetty.com/en
}

bloggers_feeds =  %w{
  http://www.atlantic-pacific.blogspot.com.br/feeds/posts/default
  http://www.blogdapaulinha.com.br/?feed=atom
  http://www.blogdamariasophia.com/feeds/posts/default
  http://www.blogdathassia.com.br/br/feed/
  http://www.comdrama.com.br/feed/
  http://www.fashiontoast.com/feed/atom
  http://www.fashionvibe.net/feeds/posts/default
  http://www.lalarudge.com.br/feed/
  http://www.lasmimas.com.br/blog/feed
  http://www.manrepeller.com/feed
  http://www.songofstyle.blogspot.com/feeds/posts/default
  http://www.blogdamariah.com.br/index.php/feed/
  http://www.glam4you.com/feed/
  http://www.garotasestupidas.com/feed/
  http://www.lalanoleto.com.br/feed/atom/
  http://www.hanneli.com/feed/
  http://feeds.feedblitz.com/cupcakesandcashmere
  http://www.theblondesalad.com/feed
  http://www.tuulavintage.com/feed/
  http://www.thecherryblossomgirl.com/feed/atom/
  http://www.stylescrapbook.com/feeds/posts/default
  http://leblogdebetty.tumblr.com/rss
}

########################################################
street_style_sites = %w{
  http://www.carolinesmode.com/stockholmstreetstyle/
  http://www.glamour.tumblr.com/tagged/street-style
  http://www.facehunter.blogspot.com.br/
  http://www.hel-looks.com/
  http://www.thelocals.dk/category/people/women/
  http://www.streetpeeper.com/
  http://www.glamourmagazine.co.uk/
  http://www.bostonstreetstyle.com/
}

street_style_feeds = %w{
 http://www.carolinesmode.com/stockholmstreetstyle/rss/ 
 http://glamour.tumblr.com/rss
 http://feeds.feedburner.com/yvanthefacehunter
 http://feeds.feedburner.com/hel-looks
 http://feeds.feedburner.com/thelocals/feed
 http://streetpeeper.com/fashion-feed.xml
 http://www.glamourmagazine.co.uk/rss/
 http://www.bostonstreetstyle.com/rss
}

#######################################################

celebrities_sites = %w{ http://www.outfitidentifier.com/ }
celebrities_feeds = %w{ http://www.outfitidentifier.com/?feed=atom }

#######################################################

categories = [
  {
    name: 'Bloggers',
    sites: bloggers_sites,
    feeds: bloggers_feeds
  },
  {
    name: 'Street Style',
    sites: street_style_sites,
    feeds: street_style_feeds
  },
  {
    name: 'Celebrities',
    sites: celebrities_sites,
    feeds: celebrities_feeds
  } 
]


@conn = Faraday.new(:url => ENV['LOOK_API_ROOT']) do |faraday|
  faraday.request  :url_encoded             # form-encode POST params
  faraday.response :logger                  # log requests to STDOUT
  faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
end

categories.each do |cat|
  @conn.post('/1/categories.json',category_name: cat[:name])

  cat[:sites].each_with_index.map do |url,i|
    feed = Feedzirra::Feed.fetch_and_parse(cat[:feeds][i])
    if feed.class != Fixnum #TODO: Verify the ones that are Bugged
      Source.create!({
        title: feed.title, 
        category: cat[:name],
        feed_url: cat[:feeds][i]
      })
      @conn.post('/1/sources.json',{
        source: {
          title: feed.title,
          name: url.gsub(/http:\/\/www.|.com.*|.blogspot.*|.co.*|.net.*|.dk.*/,''),
          thumb: 'http://fakeimg.pl/30/',
          url: {
            site: url,
            feed: cat[:feeds][i]
          }
        },
        category_name: cat[:name]
      })
    end
  end
end



##############################################################
##############################################################
#

def insert_image(content_parsed,post)
  content_parsed.xpath('//img/@src').collect do |img|
    if Post.where('images.src' => img.to_s).first.nil? and !img.to_s.empty?
      image = post.images.build(src: img.to_s)
      image.save!
    end
  end
end

Source.all.collect do |source|
  feed = Feedzirra::Feed.fetch_and_parse(source.feed_url)
  if feed.class != Fixnum #TODO: Verify the ones that are Bugged
    puts feed.url
    feed.entries.collect do |entry|
      if entry.url and entry.published
        post = Post.where(url: entry.url).first || Post.create!(url: entry.url, published: entry.published)
      end
      if post
        if PICS_ON_SUMMARY.include?(feed.url)
          insert_image(Nokogiri::HTML(entry.summary),post)
        end
        if feed.url == 'http://www.manrepeller.com'
          conn = Faraday.new(url: entry.url)
          insert_image(Nokogiri::HTML(conn.get.body).search('#slidebox'),post)
        end
        if feed.url == 'http://streetpeeper.com/fashion-feed.xml'
          Nokogiri::HTML(entry.summary).xpath('//img/@src').collect do |img|
            img = img.to_s.insert(0,'http://streetpeeper.com')
            if Post.where('images.src' => img).first.nil? and !img.empty?
              image = post.images.build(src: img)
              image.save!
            end
          end
        else
          insert_image(Nokogiri::HTML(entry.content),post)
        end
        post.save!
        source.posts.push(post)
        source.save!
        puts "With: #{post.images.count} images"
      end
    end
    puts "Posts created: #{source.posts.count}"
  end
end


puts "Created #{Post.count} posts"
# REMOVED: (feed not found)
# http://www.vogue.co.uk/spy/street-chic
# http://www.coggles.com/street-style
# http://stockholmstreetstyle.feber.se


