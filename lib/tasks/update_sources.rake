PICS_ON_SUMMARY = [
  'http://glamour.tumblr.com/',
  'http://facehunter.blogspot.com/',
  'http://hel-looks.com',
  'http://thelocals.dk',              
  'http://www.bostonstreetstyle.com/',
  'http://leblogdebetty.tumblr.com/'
]

namespace :sources do
  task :get => :environment do
    def insert_image(content_parsed,post)
      content_parsed.xpath('//img/@src').collect do |img|
        if Post.where('images.src' => img.to_s).first.nil? and !img.to_s.empty?
          image = post.images.build(src: img.to_s)
          image.save!
        end
      end
    end

    Source.all.collect do |source|
      feed = Feedzirra::Feed.fetch_and_parse(source.feed_url)
      if feed.class != Fixnum #TODO: Verify the ones that are Bugged
        puts feed.url
        feed.entries.collect do |entry|
          if entry.url and entry.published
            post = Post.where(url: entry.url).first || Post.create!(url: entry.url, published: entry.published)
          end
          if post
            if PICS_ON_SUMMARY.include?(feed.url)
              insert_image(Nokogiri::HTML(entry.summary),post)
            end
            if feed.url == 'http://www.manrepeller.com'
              conn = Faraday.new(url: entry.url)
              insert_image(Nokogiri::HTML(conn.get.body).search('#slidebox'),post)
            end
            if feed.url == 'http://streetpeeper.com/fashion-feed.xml'
              Nokogiri::HTML(entry.summary).xpath('//img/@src').collect do |img|
                img = img.to_s.insert(0,'http://streetpeeper.com')
                if Post.where('images.src' => img).first.nil? and !img.empty?
                  image = post.images.build(src: img)
                  image.save!
                end
              end
            else
              insert_image(Nokogiri::HTML(entry.content),post)
            end
            post.save!
            source.posts.push(post)
            source.save!
            puts "With: #{post.images.count} images"
          end
        end
        puts "Posts created: #{source.posts.count}"
      end
    end
  end
end
